<?php
/**
 * @file
 * FedEx address validation.
 */

$plugin = array(
  'title' => t('Spanish Address validation, user submit'),
  'format callback' => 'spanish_address_validation_format_submit_callback',
  'type' => 'address',
  'weight' => -100,
);

/**
 * Format callback.
 *
 * @see CALLBACK_addressfield_validate_callback()
 */
function spanish_address_validation_format_submit_callback(&$format, $address, $context = array()) {
  if ($context['mode'] == 'form') {
    $format['#element_validate'][] = 'spanish_address_validation_element_submit_validate';
  }
}

Spanish Postal Code Validation



This module provides a function to validate spanish addresses.

It is a extension of AddressField (https://www.drupal.org/project/addressfield)
used in commerce to store customer Addresses.

This module validate that the postal_code is valid and correspond to the right
Spanish administrative area.

REQUIREMENTS
------------

This module requires the following modules:

 * Address Field (https://www.drupal.org/project/addressfield)

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.

CONFIGURATION
-------------

 * Edit Address field of your content type or commerce customer profile
 (admin/commerce/customer-profiles/types) and select "Spanish Address
 validation, user submit" plugin.

MAINTAINERS
-----------

Current Maintainer:
  * David Gil (davidgil) - https://www.drupal.org/u/davidgil
